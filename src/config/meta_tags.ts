const app_url = 'https://biolink-delta.vercel.app/';

const meta_tags = {
	app_url,
	title: 'Biolink',
	description:
		'A landing page intended to act as a gateway to my other interconnected sites .',
	keywords:
		'linktree, links, social, link-in-bio, nextjs, tailwind, react, typescript, js, ts, tsx, jsx',
	author: 'Thomas Leon Highbaugh',
	og_image_url: `${app_url}/hero-image.png`,
};

export default meta_tags;
